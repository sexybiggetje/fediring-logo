# fediring-logo

A silly attempt at making a colorful [Fediring](https://fediring.net) logo. Fediring is an old-school webring for personal sites of members of the fediverse. When subscribing to the ring you register your fediverse handle and website, which in turns allows you to navigate the previous and next website in the ring.

Fediring contains rings for the plain web, and Gemini. If you wish to learn more about it, please look for information on [the website](https://fediring.net).

| Size | Image |
| ------- | ------- |
| 256x256 | <a href="https://fediring.net"><img alt="fediring.net" src="https://codeberg.org/sexybiggetje/fediring-logo/raw/branch/main/fediring.min.svg" width="256"></a> |
| 128x128 | <a href="https://fediring.net"><img alt="fediring.net" src="https://codeberg.org/sexybiggetje/fediring-logo/raw/branch/main/fediring.min.svg" width="128"></a> |
| 64x64 | <a href="https://fediring.net"><img alt="fediring.net" src="https://codeberg.org/sexybiggetje/fediring-logo/raw/branch/main/fediring.min.svg" width="64"></a> |


# Files and versions explained
The file fediring.svg is created using [Inkscape](https://inkscape.org/), and thus contains a lot of metadata. Therefore it's better to use the minified version, fediring.min.svg; the latter image is compressed with [Vecta Nano](https://vecta.io/nano).

# Embedding the logo
The canvas is set to 512x512px, but it is easily displayed at a recommended size of 128x128px; You can use any dimension, but it looks best if the resolution is dividable by 8.

```html
<a href="https://fediring.net">
    <img alt="fediring.net" src="https://codeberg.org/sexybiggetje/fediring-logo/raw/branch/main/fediring.min.svg" width="128">
</a>
```